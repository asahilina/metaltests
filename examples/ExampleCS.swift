import Metal

let shader = """
#include <metal_stdlib>
using namespace metal;

kernel void test(uint pos [[thread_position_in_grid]], device uint* optr, const device uint* iptr) {
	optr[pos] = iptr[pos] * 2;
}
"""

for gpu in MTLCopyAllDevices() {
	let lib = try gpu.makeLibrary(source: shader, options: nil)
	let data: [UInt32] = [0, 1, 2, 3]
	let size = data.count
	let obuf = gpu.makeBuffer(length: size * MemoryLayout<UInt32>.size)!
	let ibuf = gpu.makeBuffer(length: size * MemoryLayout<UInt32>.size)!
	ibuf.contents().initializeMemory(as: UInt32.self, from: data, count: size)
	let pipe = try gpu.makeComputePipelineState(function: lib.makeFunction(name: "test")!)
	let q = gpu.makeCommandQueue()!
	do {
		let cb = q.makeCommandBuffer()!
		let enc = cb.makeComputeCommandEncoder()!
		enc.setBuffer(obuf, offset: 0, index: 0)
		enc.setBuffer(ibuf, offset: 0, index: 1)
		enc.setComputePipelineState(pipe)
		enc.dispatchThreadgroups(
			MTLSize(width: max(size/32, 1), height: 1, depth: 1),
			threadsPerThreadgroup: MTLSize(width: min(size, 32), height: 1, depth: 1)
		)
		enc.endEncoding()
		cb.commit()
		cb.waitUntilCompleted()
	}
	let optr = obuf.contents().bindMemory(to: UInt32.self, capacity: size)
	for i in 0..<size {
		print(optr[i])
	}
}
