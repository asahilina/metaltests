#!/usr/bin/env swift

import Metal

typealias FunctionPrototype = @convention(c) (_: UInt32, _: UInt64, _: UInt64, _: UInt64, _: UInt64) -> Void

let handle = dlopen("../hvcall.dylib", RTLD_NOW)!
let symbol = dlsym(handle, "hvcall")
let hvcall = unsafeBitCast(symbol, to: FunctionPrototype.self)

func start_tracing() {
    hvcall(100, 0, 0, 0, 0)
}

func stop_tracing() {
    hvcall(101, 0, 0, 0, 0)
}

start_tracing()
defer { stop_tracing() }

let shader = """
#include <metal_stdlib>
using namespace metal;

vertex float4 vs(uint vid [[vertex_id]]) {
	return float4(vid & 1 ? 3 : -1, vid & 2 ? 3 : -1, 0, 1);
}

fragment float4 fs(float4 pos [[position]], texture2d<float> tex [[texture(0)]]) {
	return tex.read(uint2(pos.xy)) * 2;
}
"""

func triggerProgrammaticCapture(device: MTLDevice) {
    let captureManager = MTLCaptureManager.shared()
    let captureDescriptor = MTLCaptureDescriptor()
	print(captureManager.supportsDestination(.gpuTraceDocument))
    captureDescriptor.captureObject = device
    captureDescriptor.destination = .gpuTraceDocument
    captureDescriptor.outputURL = URL(fileURLWithPath: "/tmp/perf.gputrace")
    do {
        try captureManager.startCapture(with: captureDescriptor)
    } catch {
        fatalError("error when trying to capture: \(error)")
    }
}


func runMetalCommands(commandQueue: MTLCommandQueue) {
    let commandBuffer = commandQueue.makeCommandBuffer()!
    // Do Metal work.
    commandBuffer.commit()
    let captureManager = MTLCaptureManager.shared()
    captureManager.stopCapture()
}

for gpu in MTLCopyAllDevices() {
	triggerProgrammaticCapture(device: gpu)

	let dims = MTLSize(width: 2, height: 2, depth: 1)
	let size = dims.width * dims.height
	let fmt = MTLPixelFormat.rgba8Unorm
	let bpp = 4
	let lib = try gpu.makeLibrary(source: shader, options: nil)
	let ibuf = gpu.makeBuffer(length: size * bpp)!
	let iptr = ibuf.contents().bindMemory(to: UInt32.self, capacity: size)
	iptr[0] = 0xff112233
	iptr[1] = 0xff445566
	iptr[2] = 0xff778899
	iptr[3] = 0xffaabbcc
	let tdesc = MTLTextureDescriptor.texture2DDescriptor(pixelFormat: fmt, width: dims.width, height: dims.height, mipmapped: false)
	tdesc.storageMode = .private
	tdesc.usage = .renderTarget
	let otex = gpu.makeTexture(descriptor: tdesc)!
	tdesc.usage = .shaderRead
	let itex = gpu.makeTexture(descriptor: tdesc)!

	let pdesc = MTLRenderPipelineDescriptor()
	pdesc.colorAttachments[0].pixelFormat = fmt
	pdesc.vertexFunction = lib.makeFunction(name: "vs")!
	pdesc.fragmentFunction = lib.makeFunction(name: "fs")!
	let pipe = try gpu.makeRenderPipelineState(descriptor: pdesc)

	let q = gpu.makeCommandQueue()!
	do {
		let cb = q.makeCommandBuffer()!
		do {
			let rpdesc = MTLRenderPassDescriptor()
			rpdesc.colorAttachments[0].loadAction = .clear
			rpdesc.colorAttachments[0].clearColor = MTLClearColor(red: 1, green: 1, blue: 1, alpha: 1)
			rpdesc.colorAttachments[0].storeAction = .store
			rpdesc.colorAttachments[0].texture = otex
			let enc = cb.makeRenderCommandEncoder(descriptor: rpdesc)!
			enc.setFragmentTexture(itex, index: 0)
			enc.setRenderPipelineState(pipe)
			enc.drawPrimitives(type: .triangle, vertexStart: 0, vertexCount: 372)
			enc.endEncoding()
		}
		cb.commit()
		cb.waitUntilCompleted()
	}
    let captureManager = MTLCaptureManager.shared()
    captureManager.stopCapture()
}
