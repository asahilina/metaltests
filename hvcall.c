#include <stdint.h>

asm(
        ".align 4\n"
        ".globl _hvcall\n"
        "_hvcall:\n"
        "       brk #0x4242\n"
        "       ret\n"
);

int hvcall(uint32_t callid, uint64_t arg1, uint64_t arg2, uint64_t arg3, uint64_t arg4);
