// Offsets for macOS 13.5 Ventura

import Metal
import Darwin

extension String.StringInterpolation {
	mutating func appendInterpolation<T: BinaryInteger>(hex value: T, width: Int = 0) {
		var str = String(value, radix: 16, uppercase: true)
		if str.count < width {
			let pad = String(repeating: "0", count: width - str.count)
			if str.hasPrefix("-") {
				str = "-\(pad)\(str.dropFirst())"
			} else {
				str = pad + str
			}
		}
		appendInterpolation(str)
	}
}

func bridgeRetained<T : AnyObject>(obj : T) -> UnsafeRawPointer {
    return UnsafeRawPointer(Unmanaged.passRetained(obj).toOpaque())
}

func div_round_up(_ a: Int, _ b: Int) -> Int {
	return (a + b - 1) / b
}

func next_pot(_ a: Int) -> Int {
	return 1 << Int(ceil(log2(Float(a))))
}

func testTilesize(cpup: UnsafeRawPointer, mip: Int, off: Int, bound: Int,
				  w_el: Int, h_el: Int, stride_el: Int,
				  blk_size: Int, ts: Int) -> Bool {
	let logth_el = ts / 2
	let logtw_el = (ts + 1) / 2
	let tw_el = 1 << logtw_el
	let ts_el = 1 << ts
	let w_ti = div_round_up(stride_el, tw_el)

	var logx_el = 0
// 	fputs("Test tile size \(ts): log \(logtw_el)x\(logth_el) tile stride: \(w_ti)\n", stderr)
	while (1 << logx_el) < w_el {
		let tx_ti = (1 << logx_el) / (1 << logtw_el)
		let toff_el = logtw_el <= logx_el ? 0 : 1 << (logx_el * 2)

		let off_blk = ts_el * tx_ti + toff_el
		let load_off = off + off_blk * blk_size
		if (load_off + 4) >= bound {
			return false
		}
		let tmip = cpup.load(fromByteOffset: load_off, as: UInt8.self)
		let tflg = cpup.load(fromByteOffset: load_off + 1, as: UInt8.self)
		let tx_el = cpup.load(fromByteOffset: load_off + 2, as: UInt16.self)
// 		fputs(" X @\(off_blk)(\(toff_el)) \(tmip) \(tflg) \(tx_el) == \(1 << logx_el)\n", stderr)

		if tmip != mip || tflg != 0xaa || tx_el != (1 << logx_el) {
			return false
		}
		logx_el += 1
	}

	var logy_el = 0
	while (1 << logy_el) < h_el {
		let ty_ti = (1 << logy_el) / (1 << logth_el)
		let toff_el = logth_el <= logy_el ? 0 : 1 << (logy_el * 2 + 1)

		let off_blk = (ts_el * ty_ti * w_ti) + toff_el
		let load_off = off + off_blk * blk_size
		if (load_off + 4) >= bound {
			return false
		}
		let tmip = cpup.load(fromByteOffset: load_off, as: UInt8.self)
		let tflg = cpup.load(fromByteOffset: load_off + 1, as: UInt8.self)
		let ty_el = cpup.load(fromByteOffset: load_off + 2, as: UInt16.self)
// 		fputs(" Y @\(off_blk)(\(toff_el)) \(tmip) \(tflg) \(ty_el) == \(1 << logy_el)\n", stderr)

		if tmip != mip || tflg != 0x55 || ty_el != (1 << logy_el) {
			return false
		}
		logy_el += 1
	}

	//fputs("=> Valid\n", stderr)

	return true
}

func test(gpu:MTLDevice, width: Int, height: Int, format: MTLPixelFormat, pipe_fmt: String,
		  blk_size: Int, bw: Int, bh: Int) {
	let heightLevels = Int(floor(log2(Float(height))) + 1)
	let widthLevels = Int(floor(log2(Float(width))) + 1)
	let mipLevels = (heightLevels > widthLevels) ? heightLevels : widthLevels

	let desc = MTLTextureDescriptor()
	desc.width = width
	desc.height = height
	desc.mipmapLevelCount = mipLevels
	desc.pixelFormat = format
	// force off compression
	//desc.usage = .unknown

	let tex = gpu.makeTexture(descriptor: desc)!
	let ptr = bridgeRetained(obj: tex)

	let gpuva = ptr.load(fromByteOffset: 8 * 12, as: Int64.self)
	let cpup = ptr.load(fromByteOffset: 8 * 19, as: UnsafeRawPointer.self)
	let alloc_len = ptr.load(fromByteOffset: 8 * 8, as: Int64.self)
	let real_len = ptr.load(fromByteOffset: 8 * 63, as: Int64.self)
	let flags = ptr.load(fromByteOffset: 8 * 40, as: Int64.self)

	let is_compressed = flags & 0x10000000000 != 0

// 	for i in 0..<256 {
// 		let v = ptr.load(fromByteOffset: 8 * i, as: Int64.self)
// 		fputs("\(i): 0x\(String(v, radix: 16, uppercase: true))\n", stderr)
// 	}

	fputs("Texture @ \(ptr) (\(width)x\(height))\n", stderr)
	fputs("Flags: 0x\(hex: flags)\n", stderr)
	fputs("CPU VA: \(cpup)\n", stderr)
	fputs("GPU VA: 0x\(hex: gpuva)\n", stderr)
	fputs("Alloc len: 0x\(hex: alloc_len)\n", stderr)
	fputs("Real len: 0x\(hex: real_len)\n", stderr)
	fputs("Is compressed: \(is_compressed)\n", stderr)

	if is_compressed {
		let sub = ptr.load(fromByteOffset: 8 * 65, as: UnsafeRawPointer.self)
		let gpuva_cbuf = sub.load(fromByteOffset: 8 * 52, as: Int64.self) << 4
		let cbuf_off = gpuva_cbuf - gpuva
		let cbuf_size = real_len - cbuf_off
		fputs("Comp VA: 0x\(hex: gpuva_cbuf)\n", stderr)
		fputs("Comp off: 0x\(hex: cbuf_off)\n", stderr)
		fputs("Comp size: 0x\(hex: cbuf_size)\n", stderr)
	}

	for mip in 0..<mipLevels {
		let dw = max(1, width >> mip)
		let dh = max(1, height >> mip)
		let bx = div_round_up(dw, bw)
		let by = div_round_up(dh, bh)
		var data = [UInt8](repeating: 0, count: Int(bx * by * blk_size))
		let region: MTLRegion = MTLRegionMake2D(0, 0, dw, dh)

		var ix = 0
		while ix < bx {
			let off = blk_size * ix
			data[off] = UInt8(mip)
			data[off + 1] = 0xaa
			data[off + 2] = UInt8(ix & 0xff)
			data[off + 3] = UInt8(ix >> 8)
			if ix == 0 {
				ix = 1
			} else {
				ix *= 2
			}
		}
		var iy = 1
		while iy < by {
			let off = blk_size * bx * iy
			data[off] = UInt8(mip)
			data[off + 1] = 0x55
			data[off + 2] = UInt8(iy & 0xff)
			data[off + 3] = UInt8(iy >> 8)
			iy *= 2
		}

		tex.replace(region: region, mipmapLevel: mip,
					withBytes: data, bytesPerRow: bx * blk_size)
	}

	var mips: [Int] = []
	var mip: Int8 = -1
	for p in 0..<(Int(real_len) / 0x80) {
		let data = cpup.load(fromByteOffset: 0x80 * p, as: Int8.self)
		if data == (mip + 1){
			mips.append(0x80 * p)
			mip = data;
			fputs("Mip \(mip) @ 0x\(hex: p)\n", stderr)
		}
		if mip >= (mipLevels - 1) {
			break
		}
	}

	var tile_sizes: [Int] = []
	for mip in 0..<mipLevels {
		let off = mips[mip]
		let bound = mips.count == (mip + 1) ? Int(real_len) : mips[mip + 1]
		let w_el = div_round_up(max(1, width >> mip), bw)
		let h_el = div_round_up(max(1, height >> mip), bh)
		fputs("Test tile size for \(mip) -> \(w_el)x\(h_el)\n", stderr)

		var valid_ts = 0
		for ts in 0...14 {
			let th_el = 1 << (ts / 2)
			let tw_el = 1 << ((ts + 1) / 2)

			if testTilesize(cpup: cpup, mip: mip, off: off, bound: bound,
							w_el: w_el, h_el: h_el, stride_el: w_el,
							blk_size: blk_size, ts: ts) {
				valid_ts |= (1 << ts)
			}
			if tw_el > w_el && th_el > h_el {
				break
			}
		}

		/* Compressed formats can have extra stride padding */
		let stride_el = w_el + 1
		fputs("Test tile size for \(mip) -> \(w_el)x\(h_el) @\(stride_el)\n", stderr)
		for ts in 0...14 {
			let th_el = 1 << (ts / 2)
			let tw_el = 1 << ((ts + 1) / 2)

			if testTilesize(cpup: cpup, mip: mip, off: off, bound: bound,
							w_el: w_el, h_el: h_el, stride_el: stride_el,
							blk_size: blk_size, ts: ts) {
				valid_ts |= (1 << (ts + 16))
			}
			if tw_el > w_el && th_el > h_el {
				break
			}
		}

		if valid_ts == 0 {
			fputs("No valid tile size found for \(w_el)x\(h_el) (block \(blk_size))\n", stderr)
			let cnt = (bound - off) / blk_size
			for i in 0..<cnt {
				let v = cpup.load(fromByteOffset: off + blk_size * i, as: Int32.self)
				print("\(i): 0x\(hex: v)")
			}
			exit(1);
		}
		tile_sizes.append(valid_ts)
	}

	fputs("/* \(pipe_fmt) \(width)x\(height) */\n", stdout)
	fputs("{\n", stdout)
	fputs("   \(pipe_fmt),\n", stdout)
	fputs("   \(width),\n", stdout)
	fputs("   \(height),\n", stdout)
	fputs("   \(mipLevels),\n", stdout)
	fputs("   {\n", stdout)
	for off in mips {
		fputs("      0x\(hex: off),\n", stdout)
	}
	fputs("   },\n", stdout)
	fputs("   {\n", stdout)
	for valid_ts in tile_sizes {
		fputs("      0x\(hex: valid_ts),\n", stdout)
	}
	fputs("   }\n", stdout)
	fputs("},\n", stdout)
	fflush(stdout)
}

let gpu = MTLCopyAllDevices()[0]


let dimensions = [
	1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17,
	18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33,
	60, 63, 64, 65, 68,
	90, 95, 96, 97, 102,
	115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127,
	128,
	129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141,
	243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255,
	256,
	257, 258, 259, 260, 261, 262, 263, 264, 265, 266, 267, 268, 269,
	507, 508, 509, 510, 511, 512, 513, 514, 515, 516, 517,
	1023, 1024, 1025,
]

for (format, pipe_fmt, bw, bh, blk_size) in [
	(MTLPixelFormat.rgba8Unorm, "PIPE_FORMAT_R8G8B8A8_UNORM", 1, 1, 4),
	(MTLPixelFormat.bc1_rgba, "PIPE_FORMAT_DXT1_RGBA", 4, 4, 8),
	(MTLPixelFormat.bc2_rgba, "PIPE_FORMAT_DXT3_RGBA", 4, 4, 16),
	(MTLPixelFormat.astc_4x4_srgb, "PIPE_FORMAT_ASTC_4x4_SRGB", 4, 4, 16),
	(MTLPixelFormat.astc_5x4_srgb, "PIPE_FORMAT_ASTC_5x4_SRGB", 5, 4, 16),
	(MTLPixelFormat.astc_5x5_srgb, "PIPE_FORMAT_ASTC_5x5_SRGB", 5, 5, 16),
	(MTLPixelFormat.astc_6x5_srgb, "PIPE_FORMAT_ASTC_6x5_SRGB", 6, 5, 16),
	(MTLPixelFormat.astc_6x6_srgb, "PIPE_FORMAT_ASTC_6x6_SRGB", 6, 6, 16),
	(MTLPixelFormat.astc_8x5_srgb, "PIPE_FORMAT_ASTC_8x5_SRGB", 8, 5, 16),
	(MTLPixelFormat.astc_8x6_srgb, "PIPE_FORMAT_ASTC_8x6_SRGB", 8, 6, 16),
	(MTLPixelFormat.astc_8x8_srgb, "PIPE_FORMAT_ASTC_8x8_SRGB", 8, 8, 16),
	(MTLPixelFormat.astc_10x5_srgb, "PIPE_FORMAT_ASTC_10x5_SRGB", 10, 5, 16),
	(MTLPixelFormat.astc_10x6_srgb, "PIPE_FORMAT_ASTC_10x6_SRGB", 10, 6, 16),
	(MTLPixelFormat.astc_10x8_srgb, "PIPE_FORMAT_ASTC_10x8_SRGB", 10, 8, 16),
	(MTLPixelFormat.astc_10x10_srgb, "PIPE_FORMAT_ASTC_10x10_SRGB", 10, 10, 16),
	(MTLPixelFormat.astc_12x10_srgb, "PIPE_FORMAT_ASTC_12x10_SRGB", 12, 10, 16),
	(MTLPixelFormat.astc_12x12_srgb, "PIPE_FORMAT_ASTC_12x12_SRGB", 12, 12, 16),
] {
	for width in dimensions {
		for height in dimensions {
			test(gpu: gpu, width:width, height:height, format:format, pipe_fmt:pipe_fmt, blk_size:blk_size, bw: bw, bh: bh)
		}
	}
}
