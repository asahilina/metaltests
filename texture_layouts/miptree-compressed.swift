// Offsets for macOS 13.5 Ventura

import Metal
import Darwin

func bridgeRetained<T : AnyObject>(obj : T) -> UnsafeRawPointer {
    return UnsafeRawPointer(Unmanaged.passRetained(obj).toOpaque())
}

func test(gpu:MTLDevice, width: Int, height: Int, format: MTLPixelFormat, pipe_fmt: String,
		  blk_size: Int, bw: Int, bh: Int) {
	let heightLevels = Int(floor(log2(Float(height))) + 1)
	let widthLevels = Int(floor(log2(Float(width))) + 1)
	let mipLevels = (heightLevels > widthLevels) ? heightLevels : widthLevels

	let desc = MTLTextureDescriptor()
	desc.width = width
	desc.height = height
	desc.mipmapLevelCount = mipLevels
	desc.pixelFormat = format
	// force off compression
	desc.usage = MTLTextureUsage.unknown

	let tex = gpu.makeTexture(descriptor: desc)!
	let ptr = bridgeRetained(obj: tex)

	let gpuva = ptr.load(fromByteOffset: 8 * 12, as: Int64.self)
	let cpup = ptr.load(fromByteOffset: 8 * 19, as: UnsafeRawPointer.self)
	let alloc_len = ptr.load(fromByteOffset: 8 * 8, as: Int64.self)
	let real_len = ptr.load(fromByteOffset: 8 * 63, as: Int64.self)
	let flags = ptr.load(fromByteOffset: 8 * 40, as: Int64.self)

	let is_compressed = flags & 0x10000000000 != 0

// 	for i in 0..<256 {
// 		let v = ptr.load(fromByteOffset: 8 * i, as: Int64.self)
// 		fputs("\(i): 0x\(String(v, radix: 16, uppercase: true))", stderr)
// 	}

	fputs("Texture @ \(ptr)\n", stderr)
	fputs("Flags: 0x\(String(flags, radix: 16, uppercase: true))\n", stderr)
	fputs("CPU VA: \(cpup)\n", stderr)
	fputs("GPU VA: 0x\(String(gpuva, radix: 16, uppercase: true))\n", stderr)
	fputs("Alloc len: 0x\(String(alloc_len, radix: 16, uppercase: true))\n", stderr)
	fputs("Real len: 0x\(String(real_len, radix: 16, uppercase: true))\n", stderr)
	fputs("Is compressed: \(is_compressed)\n", stderr)

	if is_compressed {
		let sub = ptr.load(fromByteOffset: 8 * 65, as: UnsafeRawPointer.self)
		let gpuva_cbuf = sub.load(fromByteOffset: 8 * 52, as: Int64.self) << 4
		let cbuf_off = gpuva_cbuf - gpuva
		let cbuf_size = real_len - cbuf_off
		fputs("Comp VA: 0x\(String(gpuva_cbuf, radix: 16, uppercase: true))\n", stderr)
		fputs("Comp off: 0x\(String(cbuf_off, radix: 16, uppercase: true))\n", stderr)
		fputs("Comp size: 0x\(String(cbuf_size, radix: 16, uppercase: true))\n", stderr)
	}

	for mip in 0..<mipLevels {
		let data = [UInt8](repeating: UInt8(mip), count: Int(real_len * 16))
		let dw = max(1, width >> mip)
		let dh = max(1, height >> mip)
		let bx = (dw + bw - 1) / bw
		let region: MTLRegion = MTLRegionMake2D(0, 0, dw, dh)
		tex.replace(region: region, mipmapLevel: mip,
					withBytes: data, bytesPerRow: bx * blk_size)
	}

// 	for i in 0..<256 {
// 		let v = cpup.load(fromByteOffset: 8 * i, as: Int64.self)
// 		print("\(i): 0x\(String(v, radix: 16, uppercase: true))")
// 	}

	var mips: [Int] = []
	var mip: Int8 = -1
	for p in 0..<Int(real_len) {
		let data = cpup.load(fromByteOffset: p, as: Int8.self)
		if data == (mip + 1){
			mips.append(p)
			mip = data;
			fputs("Mip \(mip) @ 0x\(String(p, radix: 16, uppercase: true))\n", stderr)
		}
		if mip >= (mipLevels - 1) {
			break
		}
	}

	fputs("{\n", stdout)
	fputs("   \(pipe_fmt),\n", stdout)
	fputs("   \(width),\n", stdout)
	fputs("   \(height),\n", stdout)
	fputs("   \(mipLevels),\n", stdout)
	fputs("   {\n", stdout)
	for off in mips {
		fputs("      0x\(String(off, radix: 16, uppercase: true)),\n", stdout)
	}
	fputs("   }\n", stdout)
	fputs("},\n", stdout)
	fflush(stdout)
}

let gpu = MTLCopyAllDevices()[0]

let dimensions = [
	1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17,
	31, 32, 33,
	63, 64, 65,
	95, 96, 97,
	126, 127, 128, 129, 130,
	252, 253, 254, 255, 256, 257, 258, 259, 260,
]

for (format, pipe_fmt, bw, bh, blk_size) in [
	(MTLPixelFormat.etc2_rgb8, "PIPE_FORMAT_ETC2_RGB8", 4, 4, 8),
	(MTLPixelFormat.bc1_rgba, "PIPE_FORMAT_DXT1_RGBA", 4, 4, 8),
	(MTLPixelFormat.bc2_rgba, "PIPE_FORMAT_DXT3_RGBA", 4, 4, 16),
	(MTLPixelFormat.bc3_rgba, "PIPE_FORMAT_DXT5_RGBA", 4, 4, 16),
] {
	for width in dimensions {
		for height in dimensions {
			test(gpu: gpu, width:width, height:height, format:format, pipe_fmt:pipe_fmt, blk_size:blk_size, bw: bw, bh: bh)
		}
	}
}
