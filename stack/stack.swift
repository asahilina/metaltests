#!/usr/bin/env swift

import Metal

typealias FunctionPrototype = @convention(c) (_: UInt32, _: UInt64, _: UInt64, _: UInt64, _: UInt64) -> Void

let handle = dlopen("../hvcall.dylib", RTLD_NOW)!
let symbol = dlsym(handle, "hvcall")
let hvcall = unsafeBitCast(symbol, to: FunctionPrototype.self)

func start_tracing() {
    hvcall(100, 0, 0, 0, 0)
}

func stop_tracing() {
    hvcall(101, 0, 0, 0, 0)
}

start_tracing()
defer { stop_tracing() }

let shader = """
#include <metal_stdlib>
using namespace metal;

float4 calc_uniform(uniform<uint> v) {
	return unpack_unorm4x8_to_float(v);
}

vertex float4 vs(uint vid [[vertex_id]],
constant uniform<uint> &un_buf[[buffer(0)]]) {
	uniform<uint> v = un_buf * 13;
	float4 val = calc_uniform(v);
	return float4(vid & 1 ? 3 : -1, vid & 2 ? 3 : -1, 0, 1) + val;
}

fragment float4 fs(float4 pos [[position]], texture2d<float> tex [[texture(0)]],
constant uniform<uint> &un_buf[[buffer(0)]]) {
	uniform<uint> v = un_buf * 13;
	float4 val = calc_uniform(v);
	return val;
}
"""

for gpu in MTLCopyAllDevices() {
	let dims = MTLSize(width: 2, height: 2, depth: 1)
	let size = dims.width * dims.height
	let fmt = MTLPixelFormat.rgba8Unorm
	let bpp = 4
	let lib = try gpu.makeLibrary(source: shader, options: nil)
	let ibuf = gpu.makeBuffer(length: size * bpp)!
	let obuf = gpu.makeBuffer(length: size * bpp)!
	let iptr = ibuf.contents().bindMemory(to: UInt32.self, capacity: size)
	iptr[0] = 0xff112233
	iptr[1] = 0xff445566
	iptr[2] = 0xff778899
	iptr[3] = 0xffaabbcc
	let tdesc = MTLTextureDescriptor.texture2DDescriptor(pixelFormat: fmt, width: dims.width, height: dims.height, mipmapped: false)
	tdesc.storageMode = .private
	tdesc.usage = .renderTarget
	let otex = gpu.makeTexture(descriptor: tdesc)!
	tdesc.usage = .shaderRead
	let itex = gpu.makeTexture(descriptor: tdesc)!

	let pdesc = MTLRenderPipelineDescriptor()
	pdesc.colorAttachments[0].pixelFormat = fmt
	pdesc.vertexFunction = lib.makeFunction(name: "vs")!
	pdesc.fragmentFunction = lib.makeFunction(name: "fs")!
	let pipe = try gpu.makeRenderPipelineState(descriptor: pdesc)

	let q = gpu.makeCommandQueue()!
	do {
		let cb = q.makeCommandBuffer()!
		do {
			let enc = cb.makeBlitCommandEncoder()!
			enc.copy(
				from: ibuf,
				sourceOffset: 0,
				sourceBytesPerRow: dims.width * bpp,
				sourceBytesPerImage: size * bpp,
				sourceSize: dims,
				to: itex,
				destinationSlice: 0,
				destinationLevel: 0,
				destinationOrigin: MTLOrigin(x: 0, y: 0, z: 0)
			)
			enc.endEncoding()
		}
		do {
			let rpdesc = MTLRenderPassDescriptor()
			rpdesc.colorAttachments[0].loadAction = .clear
			rpdesc.colorAttachments[0].clearColor = MTLClearColor(red: 1, green: 1, blue: 1, alpha: 1)
			rpdesc.colorAttachments[0].storeAction = .store
			rpdesc.colorAttachments[0].texture = otex
			let enc = cb.makeRenderCommandEncoder(descriptor: rpdesc)!
			enc.setFragmentTexture(itex, index: 0)
			enc.setRenderPipelineState(pipe)
			withUnsafeBytes(of: 0x01020304 as UInt32) {
				enc.setFragmentBytes($0.baseAddress!, length: $0.count, index: 0)
			}
			enc.drawPrimitives(type: .triangle, vertexStart: 0, vertexCount: 3)
			enc.endEncoding()
		}
		do {
			let enc = cb.makeBlitCommandEncoder()!
			enc.copy(
				from: otex,
				sourceSlice: 0,
				sourceLevel: 0,
				sourceOrigin: MTLOrigin(x: 0, y: 0, z: 0),
				sourceSize: dims,
				to: obuf,
				destinationOffset: 0,
				destinationBytesPerRow: dims.width * bpp,
				destinationBytesPerImage: size * bpp
			)
			enc.endEncoding()
		}
		cb.commit()
		cb.waitUntilCompleted()
	}
	let optr = obuf.contents().bindMemory(to: UInt32.self, capacity: size)
	for i in 0..<size {
		print(String(format: "%08x", optr[i]), terminator: i % dims.width == dims.width - 1 ? "\n" : " ")
	}
}
