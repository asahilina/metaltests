#!/usr/bin/env swift

import Metal

typealias FunctionPrototype = @convention(c) (_: UInt32, _: UInt64, _: UInt64, _: UInt64, _: UInt64) -> Void

let handle = dlopen("../hvcall.dylib", RTLD_NOW)!
let symbol = dlsym(handle, "hvcall")
let hvcall = unsafeBitCast(symbol, to: FunctionPrototype.self)

func start_tracing() {
    hvcall(100, 0, 0, 0, 0)
}

func stop_tracing() {
    hvcall(101, 0, 0, 0, 0)
}

start_tracing()
defer { stop_tracing() }

let shader = """
#include <metal_stdlib>
using namespace metal;

float4 calc_uniform(uniform<uint> v) {
	return unpack_unorm4x8_to_float(v);
}

kernel void test(uint pos [[thread_position_in_grid]], device uint* optr, const device uint* iptr,
constant uniform<uint> &un_buf[[buffer(2)]]) {
	uniform<uint> v = un_buf * 13;
	float4 val = calc_uniform(un_buf);
	optr[pos] = iptr[pos] * 2 * uint(val.x);
}
"""

for gpu in MTLCopyAllDevices() {
	let lib = try gpu.makeLibrary(source: shader, options: nil)
	let data: [UInt32] = [0, 1, 2, 3]
	let size = data.count
	let obuf = gpu.makeBuffer(length: size * MemoryLayout<UInt32>.size)!
	let ibuf = gpu.makeBuffer(length: size * MemoryLayout<UInt32>.size)!
	ibuf.contents().initializeMemory(as: UInt32.self, from: data, count: size)
	let pipe = try gpu.makeComputePipelineState(function: lib.makeFunction(name: "test")!)
	let q = gpu.makeCommandQueue()!
	do {
		let cb = q.makeCommandBuffer()!
		let enc = cb.makeComputeCommandEncoder()!
		enc.setBuffer(obuf, offset: 0, index: 0)
		enc.setBuffer(ibuf, offset: 0, index: 1)
		enc.setComputePipelineState(pipe)
		enc.dispatchThreadgroups(
			MTLSize(width: max(size/32, 1), height: 1, depth: 1),
			threadsPerThreadgroup: MTLSize(width: min(size, 32), height: 1, depth: 1)
		)
		enc.endEncoding()
		cb.commit()
		cb.waitUntilCompleted()
	}
	let optr = obuf.contents().bindMemory(to: UInt32.self, capacity: size)
	for i in 0..<size {
		print(optr[i])
	}
}
