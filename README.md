# Metal experiments

This repo contains Metal experiments to test features of Apple's GPU driver or generate test data for Mesa.

Thanks to TellowKrinkle for the examples and the general Swift help 💖
